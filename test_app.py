import unittest
import random
import sample

class TestFlask(unittest.TestCase):
    a = random.randint(0, 50000)
    b = random.randint(-1000, 1000)
    add = a+b
    sub = a-b

    def setUp(self):
        self.app = sample

    def test_add(self): 
        response = sample.app.test_client().get('/add?a={}&b={}'.format(self.a, self.b))
        self.assertEqual(self.add, int(response.get_data()))

    def test_sub(self):
        response = sample.app.test_client().get('/sub?a={}&b={}'.format(self.a, self.b))
        self.assertEqual(self.sub, int(response.get_data()))

if __name__ == "__main__":
    unittest.main()