from flask import Flask, request

app = Flask(__name__)
@app.route('/')
def index():
	return '<h1>Hello BoB from user127</h1>'

@app.route('/add')
def add_value():
	a = request.args.get('a', type=int)
	b = request.args.get('b', type=int)
	return str(a+b)

@app.route('/sub')
def sub_value():
	a = request.args.get('a', type=int)
	b = request.args.get('b', type=int)
	return str(a-b)


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8127)


